import math
import sys
import numpy as np
import matplotlib.pyplot as plt
from collections import Counter
from scipy.stats import chi2_contingency, ttest_ind


def visualize_probs(keys: list, values: list, text_name):
    bars = plt.bar(keys, values, color='skyblue')
    plt.xticks(keys, [str(k) for k in keys])

    for bar in bars:
        y_val = bar.get_height()
        plt.text(bar.get_x() + bar.get_width() / 2, y_val, round(y_val, 2), va='bottom', ha='center')

    plt.xlabel('Word length')
    plt.ylabel('p')
    plt.title(f'Word length probabilities in {text_name}')
    plt.show()

def visualize_absolute(keys: list, values: list, text_name):
    bars = plt.bar(keys, values, color='skyblue')
    plt.xticks(keys, [str(k) for k in keys])

    for bar in bars:
        y_val = bar.get_height()
        plt.text(bar.get_x() + bar.get_width() / 2, y_val, round(y_val, 2), va='bottom', ha='center')

    plt.xlabel('Word length')
    plt.ylabel('Count')
    plt.title(f'Word length count in {text_name}')
    plt.show()


if __name__ == "__main__":
    birth_day = int(sys.argv[1])
    lastname_len = len(sys.argv[2])

    X = ((birth_day * lastname_len * 23) % 20) + 1
    Y = ((X + ((birth_day * 5 + lastname_len * 7) % 19)) % 20) + 1

    text1 = ""
    text2 = ""

    with open(f"../files/{f'00{X}.txt' if X < 10 else f'0{X}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text1 = f.read()

    with open(f"../files/0{f'0{Y}.txt' if Y < 10 else f'{Y}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text2 = f.read()

    words_text1 = text1.split()
    word_lengths_text1 = [len(word) for word in words_text1]

    words_text2 = text2.split()
    word_lengths_text2 = [len(word) for word in words_text2]

    n = len(words_text1)
    m = len(words_text2)

    # Count length frequencies in texts
    length_frequency_text1 = Counter(word_lengths_text1)
    length_frequency_text2 = Counter(word_lengths_text2)

    # Visualize counts
    visualize_absolute(list(length_frequency_text1.keys()), list(length_frequency_text1.values()), "text001")
    visualize_absolute(list(length_frequency_text2.keys()), list(length_frequency_text2.values()), "text002")

    # Get length probabilities
    total_words_text1 = sum(length_frequency_text1.values())
    length_probabilities_text1 = {length: count / total_words_text1 for length, count in length_frequency_text1.items()}

    total_words_text2 = sum(length_frequency_text2.values())
    length_probabilities_text2 = {length: count / total_words_text2 for length, count in length_frequency_text2.items()}

    # Visualize probabilities
    visualize_probs(list(length_probabilities_text1.keys()), list(length_probabilities_text1.values()), "text001")
    visualize_probs(list(length_probabilities_text2.keys()), list(length_probabilities_text2.values()), "text002")

    # Compute EX and Var
    EX1 = np.mean(word_lengths_text1)
    varX1 = np.var(word_lengths_text1)
    print(f"EX1={EX1}")
    print(f"varX1={varX1}")

    EX2 = np.mean(word_lengths_text2)
    varX2 = np.var(word_lengths_text2)
    print(f"EX2={EX2}")
    print(f"varX2={varX2}")

    # Create contingency table (frequencies of lengths for each text, 0 if missing)
    lengths = sorted(set(length_frequency_text1.keys()).union(set(length_frequency_text2.keys())))
    contingency_table = np.array([
        [length_frequency_text1.get(length, 0) for length in lengths],
        [length_frequency_text2.get(length, 0) for length in lengths]
    ])

    print("CONTIGENCY TABLE\n-----------------")
    print(contingency_table)

    # Chi square test of independence
    chi2, p_value_chi2, _, _ = chi2_contingency(contingency_table)

    # Welch t-test for comparing EX values
    t_stat, p_value_ttest, df = ttest_ind(word_lengths_text1, word_lengths_text2, equal_var=False)

    sd = math.sqrt((varX1/n)+(varX2/m))
    print(f"sd {sd}")


    nd = (math.pow(sd, 4))/(((1/(n-1))*math.pow((varX1/n),2))+((1/(m-1))*math.pow((varX2/m),2)))
    print(f"nd {nd}")

    # Results
    print(f"Chi-square independence test:\nChi2 Statistic = {chi2}, p-value = {p_value_chi2}")
    print(f"Welch's t-test for equality of mean word lengths:\nT Statistic = {t_stat}, p-value = {p_value_ttest}")

    if p_value_chi2 > 0.05:
        print("[Chi square] We do not reject the null hypothesis at the alpha = 0.05 level")
    else:
        print("[Chi square] We reject the null hypothesis at the alpha = 0.05 level")

    if p_value_ttest > 0.05:
        print("[T-Test] We do not reject the null hypothesis at the alpha = 0.05 level")
    else:
        print("[T-Test] We reject the null hypothesis at the alpha = 0.05 level")

