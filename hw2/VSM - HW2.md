## VSM - HW2

Vypracovali:

- Robin Vávra
- Vít Šesták



Vstupní parametry (reprezentant - Vávra Robin, narozen v Třebíči 12. června 2000 v 9h ráno):

| Parametr | Hodnota |
| -------- | ------- |
| K        | 12      |
| L        | 5       |
| X        | 1       |
| Y        | 2       |


### Část 1

#### Zadani
Z obou datových souborů načtěte texty k analýze. Pro každý text zvlášť odhadněte pravděpodobnosti délek slov a graficky znázorněte rozdělení délek slov.

#### Vystup
Nejprve jsme zmerili pocet vyskytu jednotlivych delek slov.

![text001_word_length_count.png](text001_word_length_count.png)
![text002_word_length_count.png](text002_word_length_count.png)

Z namerenych hodnot jsme spocitali pravdepodobnosti jednotlivych delek slov. 

Pravdepodobnosti jsme spocitali jednoduchym vzoreckem ```p_x = pocet_slov_delky_x / pocet_vsech_slov```

![text001_word_length_probs.png](text001_word_length_probs.png)
![text002_word_length_probs.png](text002_word_length_probs.png)

### Část 2

#### Zadani
Pro každý text zvlášť odhadněte základní charakteristiky délek slov, tj. střední hodnotu a rozptyl. Řádně vysvětlete, jak charakteristiky odhadujete!

#### Vystup
- stredni hodnota
  - pouzili jsme vzorecek pro vypocet stredni hodnoty diskretni nahodne veliciny
  - pro nas pripad je tedy stredni hodnota rovna ```suma(pocet_slov_delky_x * pravdepodobnost_slova_delky_x)```

![EX_diskr.png](../images/EX_diskr.png)

- rozptyl
  - pocitali jsme podle vzorecku

![var.png](../images/var.png)

  - respektive podle vzorecku 

![var.png](../images/var2.png)


- vysledky (zaokrouhleno na 4 desetinna mista):

|             | Stredni hodnota (EX)  | Rozptyl (varX) |
| ----------- |-----------------------|----------------|
| text001.txt | 4.3825                | 4.1362         |
| text002.txt | 3.9792                | 3.7392         |

### Část 3

#### Zadani
Na hladině významnosti 5% otestujte hypotézu, že rozdělení délek slov nezávisí na tom, o který jde text. Určete také p-hodnotu testu. Nápověda: Lze provést testem nezávislosti v kontingenční tabulce. Řádně popište, jakou hypotézu testujete!

#### Vystup

Testujeme hypotezu H0 proti alternative HA na hladine vyznamnosti α.
- H0 - rozdeleni delek slov nezavisi na konkretnim textu
- HA - rozdeleni delek slov zavisi na konkretnim textu
- α (hladina vyznamnosti) - 5%

Hypotezu jsme otestovali testem nezavislosti v kontigencni tabulce.

|             | delka 1 | delka 2 | delka 3 | delka 4 | delka 5 | delka 6 | delka 7 | delka 8 | delka 9 | delka 10 | delka 11 | delka 12 | delka 13 | delka 14 |
| ----------- |---------|---------|---------|---------|---------|---------|---------|---------|---------|----------|----------|----------|----------|----------|
| text001.txt | 24      | 163     | 288     | 261     | 163     | 113     | 93      | 41      | 29      | 12       | 10       | 2        | 0        | 1        |
| text002.txt | 39      | 178     | 271     | 211     | 123     | 78      | 52      | 29      | 17      | 4        | 5        | 0        | 2        | 1        |

Test nezavislosti v kontingencni tabulce je statisticky test,
ktery se pouziva ke zjisteni, zda existuje vztah mezi dvema kategorialnimi promennymi.
Pro testovani hypotezy, ze rozdeleni delek slov nezavisí na tom, o který text se jedna,
pouzijeme chi-kvadrat test nezavislosti.

Test jsme provedli podle nasledujiciho vzorecku:

![independence_test.png](../images/independence_test.png)

- vysledky (zaokrouhleno na 4 desetinna mista, krome p-hodnoty)
  - pocet stupnu volnosti = 13
  - n = 2210
  - chi2 = 32.4049
  - p-hodnota = 0.0021
  - kriticka hodnota pro 13 dof na hladine vyznamnosti 5% = 22,362
  - protoze je chi2 = 32.4049 >= 22,362 (tedy lezi v kritickem oboru), zamitame hypotezu H0 oproti HA na hladine vyznamnosti 5%

### Část 4

#### Zadani
Na hladině významnosti 5% otestujte hypotézu, že se střední délky slov v obou textech rovnají. Určete také p-hodnotu testu. Řádně zdůvodněte, proč používáte zvolený test!

#### Vystup
Testujeme hypotezu H0 proti alternative HA na hladine vyznamnosti α.
- H0 - stredni delky slov se v obou textech rovnaji
- HA - stredni delky slov se v obou textech nerovnaji
- α (hladina vyznamnosti) - 5%

Z predchozich casti jsme zjistili, ze rozptyly danych rozdeleni jsou ruzne.
Protoze testujeme stredni hodnoty dvou nezavislych velicin a 
jejich rozdeleni (viz. grafy) je priblizne normalni (coz je postacujici vzhledem k pouziti testu),
rozhodli jsme se pouzit dvouvyberovy t-test s ruznymy rozptyly.  

![dvouvyberovy_t-test.png](../images/dvouvyberovy_t-test.png)

- vysledky (zaokrouhleno na 4 desetinna mista)
  - T = 4.7676
  - p-hodnota = 1.9877e-06
  - n = 1200
  - m = 1010
  - sd = 0.08455
  - nd = 2175.5
  - α / 2 = 0,0025 > p-hodnota = 1.9877e-06 => zamitame H0 oproti HA na hladine vyznamnosti 5%