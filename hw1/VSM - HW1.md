## VSM - HW1

Vypracovali:

- Robin Vávra
- Vít Šesták



Vstupní parametry (reprezentant - Vávra Robin, narozen v Třebíči 12. června 2000 v 9h ráno):

| Parametr | Hodnota |
| -------- | ------- |
| K        | 12      |
| L        | 5       |
| X        | 1       |
| Y        | 2       |



Výstupy:

|             | Entropie               | Střední délka kódu (Huffmanův kód pro text1) | Střední délka kódu (Huff. kód pro text2) |
| ----------- | ---------------------- | -------------------------------------------- | ---------------------------------------- |
| text001.txt | 4.094689728708523 bitu | 4.13765871786931 bitu                        | -                                        |
| text002.txt | 4.037379295589635 bitu | 4.115155131264916 bitu                       | 4.07816229116945 bitu                    |



Použitý software: Python

Poznámky: 

- použité symboly: *abcdefghijklmnopqrstuvwxyz0123456789!"#$%&'()+,-./:;<=>?@[\]^_`{|}~*

  - ```python
    all_symbols = string.ascii_lowercase + string.digits + string.punctuation + string.whitespace
    ```

- pro účely vizualizace jsou v grafech vynechány symboly s 0 pravděpodobností
- velká písmena byla převedena na lower case



### Část 1

Četnosti symbolů byly normalizovány pro získání jejich pravděpodobnosti

```python
# normalize frequencies to get probabilities
for key in char_freq.keys():
    char_freq[key] /= chars_count
```

*pozn.: poslední sloupec vyjadřuje hodnoty pravděpodobnosti pro mezeru*

![image-20240327203344947](C:\Users\vitek\AppData\Roaming\Typora\typora-user-images\image-20240327203344947.png)

<img src="C:\Users\vitek\AppData\Roaming\Typora\typora-user-images\image-20240327203353358.png" alt="image-20240327203353358" style="zoom:80%;" />



### Část 2

Entropie rozdělení znaků viz. sekce Výstupy výše.

```python
entropy = -sum(p * math.log2(p) for p in filtered_char_freq.values())
```



### Část 3

Pro nalezení optimálního binárního instantního kódu C pro kódování znaků textů jsme použili Huffmanův algoritmus. Z přednášek víme, že tento algoritmus konstruuje kód požadovaných vlastností (kód je optimální a zároveň střední délka kódů jím vygenerovaných je nejmenší možná).

```python
encoding = huffman.codebook(list(char_freq.items()))
```



### Část 4

Spočtené střední délky viz. sekci Výstupy výše. Výsledky odpovídají Shannonově větě, která říká, že entropie zprávy je vždy menší nebo rovna střední délce kódu při použití optimálního kódu. Vidíme také, že střední délky kódů jsou v rozmezí "entropie + 1", což splňuje větu o střední délce optimálního kódu.

Kód C pro první text není optimální pro text druhý, neboť pro druhý text jsme našli kód s menší střední délkou, a to opět Huffmanovým algoritmem.

```python
L = sum([len(code) * char_freq[char] for char, code in encoding.items()])
```

