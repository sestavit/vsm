## VSM - HW4

Vypracovali:

- Robin Vávra
- Vít Šesták



Vstupní parametry (reprezentant - Vávra Robin, narozen v Třebíči 12. června 2000 v 9h ráno):

| Parametr | Hodnota |
| -------- | ------- |
| K        | 12      |
| L        | 5       |
| X        | 1       |
| Y        | 2       |



### Část 1

#### Zadání
Simulujte jednu trajektorii {𝑁𝑡(𝜔) ∣ 𝑡 ∈ (0,10 s)}. Průběh trajektorie graficky znázorněte. *Řádně popište princip generování této trajektorie!*
#### Výstup

Počítali jsme s následujícími parametry:

| Parametr    | Hodnota |
| ----------- | ------- |
| lambda      | 10      |
| t_max       | 10      |
| Gamma shape | 2       |
| Gamma scale | 0.25    |

Postup:

1. Vygenerovali jsme časy příchodů požadavků do systému pomocí exponenciálního rozdělení s parametrem 1/lambda (=1/10)
   - začali jsme v čase t=0
   - vygenerovali jsme z exponenciálního rozdělení s parametrem 1/10 čas příchodu dalšího požadavku
   - pokud byl čas příchodu menší než t_max, tak jsme ho uložili do pole
   - takto cyklus pokračoval, dokud čas příchodu nepřesáhl t_max 
2. Dále jsme vygenerovali časy obsluhy pro každý příchozí požadavek pomocí gamma distribuce s parametry shape=2, scale=0.25 a size=#příchody
3. Následně jsme po dvojicích sečetli časy příchodů požadavků a časy jejich obsluhy a dostali jsme časy, kdy jednotlivé požadavky byly obslouženy. 
4. Vykreslili jsme v grafu pro každou událost (čas příchodu + čas obsloužení) množství požadavků v systému



Následucící obrázek znázorňuje počty zákazníků v systému v čase _t_ ∈ (0,10 s).

![image-20240508193743006](C:\Users\vitek\AppData\Roaming\Typora\typora-user-images\image-20240508193743006.png)

Kód níže zobrazuje časy příchodů požadavků a jejich počet.

```python
Arrivals:
[0.10147045076494177, 0.15346965223290732, 0.26005907686731966, 0.37607483564688016, 0.44802997124695887, 
 0.46416592584843125, 0.48859401240947936, 0.6466686396115202, 0.7319379325879452, 0.8160822244755237,
 0.8762793158414413, 0.9403345956315172, 1.1889350107186427, 1.2578404007223756, 1.2733341498926287,
 1.3117547881482043, 1.3367023271118152, 1.4107555908665255, 1.5850036921416055, 1.5989177954332419,
 1.6520725742369713, 1.8591331683139747, 1.8838476250119405, 1.9285149824851444, 2.050136262053624,
 2.0510744417147393, 2.068287175617042, 2.208960730147171, 2.292555212300891, 2.3950801347737567,
 2.4716985488452945, 2.5247943878236865, 2.5967326784628226, 2.671567435010513, 2.700523492643354,
 2.798684477785361, 2.8882633654372163, 2.8968828241491855, 3.0861041674732386, 3.210754141927781,
 3.25792643052343, 3.3894787216789513, 3.454893236612182, 3.4768125227201976, 3.5906058892454658,
 3.8074922697030407, 3.8867055705565168, 4.074913826914361, 4.216750107273718, 4.356276936741408,
 4.37225883279841, 4.377929557784867, 4.419715916557623, 4.428881804218129, 4.594158829184655,
 4.650383304176121, 4.654990547485838, 4.666196153929362, 4.7283566263128955, 4.7741073344440315,
 4.807322244104937, 4.924115918580307, 4.9599125269802675, 5.0338418539248515, 5.510600441463545, 
 5.582785320590287, 5.600573488436555, 5.627374973979651, 5.663878611477134, 5.692161113917556,
 6.169908062165545, 6.2849171453395964, 6.38152387832657, 6.454942120457665, 6.518382593537342, 
 6.638488914854371, 6.675604046831712, 6.774182282377281, 6.929612876965882, 6.966051785423473,
 7.02395881909339, 7.076294451423028, 7.171577297995501, 7.478243748543361, 7.6489355884298185,
 7.740894572583867, 7.860478134761309, 8.026814765899603, 8.04349860784818, 8.054243313599486,
 8.08034083955576, 8.192365931206414, 8.215794493054089, 8.327402635781432, 8.548187284612533,
 8.697596310012097, 8.790596133742998, 8.796338522343461, 8.826153770180804, 9.108786823659067,
 9.258727831852857, 9.374544485005556, 9.42158117267967, 9.464612441676422, 9.733841069345246,
 9.797758109363217, 9.80973650454696, 9.819401684127792, 9.956105288691239]
Num of arrivals: 109
```



Kód níže zobrazuje časy obsluh požadavků a jejich průměr, který vyšel přibližně 0,4538. Z opakovaných běhů můžeme usoudit, že střední doba obsluhy požadavku je 0,5s.

```python
Service times:
[0.10235444 0.89491453 0.16003012 0.15645239 0.40660718 0.07527864
 0.19663917 0.70456133 0.1930377  1.00864434 0.07943776 0.06994348
 0.55122501 0.35785767 0.43341422 0.53579796 0.55152707 0.4316052
 0.37271339 0.05522975 0.20568556 0.37179551 0.14698907 0.35168821
 0.81767011 0.45331926 0.19356365 0.15899134 0.40218194 0.72089986
 0.25836702 0.21499287 0.0415465  0.71844097 0.87696697 0.22073845
 0.82600171 1.12096632 0.63837425 0.0162789  1.43926125 0.42102554
 0.33865592 0.52449445 0.05051134 0.23424448 0.11528176 0.54837902
 0.44374452 1.27639542 0.18163207 0.79231854 1.15613315 0.61184477
 0.38364198 0.37789715 0.43970287 0.30548945 0.09975656 2.04313258
 0.17089155 0.59575464 0.40657885 0.23461444 0.46668361 0.23114103
 0.37301647 0.15815828 0.58971747 0.36407012 1.00488043 0.26804558
 0.16536721 0.16943083 0.82993249 0.68261129 0.78369254 0.13123838
 0.54640853 0.42033457 0.63603804 0.61575653 0.25343551 0.19017667
 0.60457292 0.51287596 0.70774289 0.20056016 0.38263129 0.20978748
 0.52370901 0.49541005 0.93236891 0.28056151 0.65702371 0.7142227
 0.51450782 0.31488549 0.22237565 0.29429599 0.66151093 0.48783689
 0.41710089 0.31426164 0.25778483 0.28627991 0.48980537 0.32459978
 0.49145018]
Mean service time: 0.45378357463524793
```



### Část 2

#### Zadání

Simulujte 𝑛=500 nezávislých trajektorií pro 𝑡 ∈ (0,100). Na základě těchto simulací odhadněte rozdělení náhodné veličiny 𝑁_100.

#### Výstup

| Parametr    | Hodnota |
| ----------- | ------- |
| lambda      | 10      |
| t_max       | 100     |
| Gamma shape | 2       |
| Gamma scale | 0.25    |

Pro 500 trajektorií, které jsme generovali výše způsobem popsaným v části 1 (krom té změny, že jsme nyní vyřadili časy obsluhy, které přesáhly t_max (=100)).

Tzn. pro každou událost (příchod požadavku, obsluha požadavku do t_max) jsme upravili počet požadavků v systému.

Pro každou trajektorii jsme vzali počet zákazníků v čase t_max=100 a hodnoty zprůměrovali.

Výsledný průměr počtu požadavký v systému nám vyšel přibližně 4.782 a rozptyl obdobné hodnoty.

```java
[5, 3, 8, 3, 8, 3, 2, 7, 4, 4, 4, 9, 4, 7, 5, 5, 5, 4, 5, 4, 9, 8, 6, 4,
 4, 5, 5, 7, 3, 6, 2, 8, 1, 5, 4, 6, 5, 3, 5, 5, 4, 7, 6, 6, 4, 5, 7, 8,
 1, 4, 3, 6, 8, 7, 8, 7, 5, 4, 5, 4, 3, 4, 3, 5, 2, 6, 3, 8, 6, 7, 2, 13,
 11, 4, 5, 7, 10, 1, 8, 4, 7, 1, 3, 1, 7, 8, 6, 6, 3, 3, 4, 4, 7, 7, 4, 5,
 1, 4, 4, 3, 3, 2, 4, 4, 4, 2, 10, 2, 5, 4, 6, 4, 1, 3, 5, 1, 1, 2, 5, 8, 
 6, 6, 3, 7, 5, 4, 2, 2, 6, 4, 8, 6, 5, 7, 4, 3, 4, 2, 4, 8, 5, 12, 3, 8,
 4, 2, 5, 3, 5, 2, 2, 4, 3, 7, 3, 6, 4, 8, 4, 3, 1, 4, 8, 8, 3, 8, 3, 2,
 5, 4, 5, 1, 1, 6, 9, 6, 4, 2, 7, 4, 5, 6, 3, 2, 5, 7, 3, 3, 8, 1, 11, 3,
 9, 9, 7, 5, 3, 3, 3, 9, 3, 3, 1, 5, 4, 2, 8, 5, 7, 2, 4, 7, 2, 10, 4, 4,
 6, 3, 10, 2, 3, 1, 6, 7, 3, 3, 6, 7, 4, 0, 5, 5, 7, 5, 2, 3, 5, 6, 4, 4,
 4, 2, 5, 6, 4, 4, 4, 6, 4, 5, 11, 4, 5, 3, 4, 6, 4, 4, 6, 6, 3, 7, 9, 4, 
 3, 8, 2, 3, 6, 13, 4, 6, 1, 4, 7, 3, 7, 5, 6, 5, 6, 3, 5, 6, 4, 5, 5, 11, 
 5, 4, 7, 5, 4, 4, 4, 6, 4, 4, 7, 5, 5, 1, 3, 3, 3, 2, 2, 5, 3, 2, 6, 3, 
 10, 7, 7, 5, 7, 7, 5, 5, 4, 3, 3, 7, 6, 6, 0, 8, 3, 7, 6, 7, 5, 7, 4, 3,
 8, 3, 3, 4, 3, 2, 7, 9, 4, 8, 5, 4, 7, 2, 0, 8, 4, 3, 3, 7, 4, 7, 2, 5, 
 2, 5, 3, 8, 6, 4, 1, 7, 5, 2, 5, 1, 1, 3, 6, 4, 1, 2, 4, 7, 5, 4, 4, 8, 3,
 5, 6, 9, 5, 5, 7, 6, 7, 3, 5, 6, 5, 3, 7, 3, 3, 4, 0, 9, 7, 2, 7, 5, 4, 7,
 3, 5, 8, 6, 3, 2, 7, 9, 5, 3, 5, 3, 6, 4, 6, 8, 9, 2, 3, 10, 2, 6, 3, 2,
 6, 1, 6, 4, 6, 6, 5, 7, 3, 6, 5, 13, 2, 8, 4, 8, 7, 9, 3, 3, 8, 8, 5, 7,
 2, 2, 4, 4, 3, 4, 4, 6, 3, 4, 9, 1, 4, 2, 4, 3, 6, 8, 7, 4, 3, 7, 4, 4,
 5, 7, 7, 8, 3, 5, 4, 7, 2, 9, 7, 6, 5, 10, 5, 4, 6, 4]
```

Z několika běhů testu můžeme odhadnout, že náhodná veličina N_100, která popisuje počet požadavků v systému v čase t_max, má normální rozdělení se střední hodnotou 5, což je rovno (lambda / (1 / střední_doba_obsluhy=0.5)). To není náhoda.

### Část 3

#### Zadání
Diskutujte, jaké je limitní rozdělení tohoto systému pro *t*→ +∞ (vizte [**přednášku 23**](https://courses.fit.cvut.cz/NI-VSM/lectures/files/NI-VSM-Lec-23-Lecture.pdf).). Pomocí vhodného testu otestujte na hladině významnosti 5 %5 %, zda výsledky simulace 𝑁100 odpovídají tomuto rozdělení.

#### Výstup

Dle přednášky má pro t→ +∞ střední počet požadavků v systému Poissonovo rozdělení s intenzitou λ / µ, což v našem případě je 10 / 2 = 5.

Tedy:

- H_o: Střední počet požadavků v systému má Poissonovo rozdělení s intenzitou 5.
- H_a: Střední počet požadavků v systému nemá Poissonovo rozdělení s intenzitou 5.

Pro otestování jsme použili 2 testy:

1. Z-test
   - použili jsme očekávanou lambdu=5, průměr našich měření N_100 a standardní odchylku.
   - z-score jsme spočítali jako: (prumer - ocekavana_lambda) / error
   - p-hodnotu jako: 2 * (1 - stats.norm.cdf(np.abs(z_score)))
2. Poisson test z knihovny scipy.stats
   - p-hondotu jsme spocetli jako: stats.poisson.sf(sample_mean, 5)

Výsledné p-hodnoty se pro jak Z-test tak Poisson test při opakovaných bězích měnily, ale vždy byly nad hranicí 0.05, takže nulovou hypotézu na hladině významnosti 5% nezamítáme oproti Ha.

Např.

Z-Score P-Value: 0.17382992389416962
Poisson P-value 0.384039345166937