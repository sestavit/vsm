import sys
import string
import nltk
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import chisquare
from matplotlib.colors import LinearSegmentedColormap

def print_transition_matrix(matrix):
    for row in matrix:
        print(row)

def visualize_heatmap(trans_matrix, keys, text_name):
    print(keys)
    colors = [(1, 1, 1), (0, 0, 0)]  # White (bright) to black (dark)
    custom_cmap = LinearSegmentedColormap.from_list('custom_cmap', colors)
    plt.xticks(ticks=np.arange(len(keys)), labels=keys)
    plt.yticks(ticks=np.arange(len(keys)), labels=keys)
    plt.title(f'Heatmap of {text_name}')

    plt.imshow(trans_matrix, cmap=custom_cmap)
    plt.show()

if __name__ == "__main__":
    birth_day = int(sys.argv[1])
    lastname_len = len(sys.argv[2])

    X = ((birth_day*lastname_len * 23) % 20) + 1
    Y = ((X + ((birth_day * 5 + lastname_len * 7) % 19)) % 20) + 1

    text1 = ""
    text2 = ""

    with open(f"../files/{f'00{X}.txt' if X < 10 else f'0{X}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text1 = f.read()

    with open(f"../files/0{f'0{Y}.txt' if Y < 10 else f'{Y}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text2 = f.read()

    all_symbols = string.ascii_lowercase + ' '

    symbol_indexes = dict.fromkeys(all_symbols)

    symbol_index = 0
    for symbol in all_symbols:
        symbol_indexes[symbol] = symbol_index
        symbol_index += 1

    pi1 = None
    chars_count = 0
    char_freq = None
    isText1 = True
    for text in [text1, text2]:
        char_freq = dict.fromkeys(all_symbols, 0)
        chars_count = 0
        # compute frequencies of characters (case-insensitive)
        for char in text:
            chars_count += 1
            if char in string.ascii_uppercase:
                char = char.lower()
            char_freq[char] += 1

        print(char_freq)

        bigrams = nltk.bigrams(text)
        bigrams_copy = nltk.bigrams(text)

        tuples_set = set(bigrams_copy)
        # print(tuples_set)

        # [print((a,b)) for (a,b) in list(bigrams)]

        tuples_dict = dict.fromkeys(tuples_set, 0)

        for bigram in bigrams:
            tuples_dict[bigram] += 1

        P = [[0] * len(all_symbols) for _ in range(len(all_symbols))]

        transitions_num = sum(tuples_dict.values())

        symbol_transition_sum = dict.fromkeys(all_symbols, 0)

        for (a, b) in tuples_dict:
            row_ix = symbol_indexes[a]
            column_ix = symbol_indexes[b]
            P[row_ix][column_ix] = tuples_dict[(a, b)]
            symbol_transition_sum[a] += tuples_dict[(a, b)]

        for r in range(len(P)):
            row_sum = sum(P[r])
            for c in range(len(P[r])):
                P[r][c] /= row_sum

        print("Transition Matrix")
        # print_transition_matrix(P)

        P_np_array = np.array(P)

        # Convert array elements to strings with 4 digits length
        formatted_arr = np.array(['{:.4f}'.format(num) for num in P_np_array.flatten()])

        # Reshape back to the original shape
        formatted_arr = formatted_arr.reshape(P_np_array.shape)

        if isText1:
            visualize_heatmap(P, list(symbol_indexes.keys()), "text001")
            np.savetxt("text001_transition_matrix.csv", formatted_arr, delimiter=",", fmt='%s')
        else:
            visualize_heatmap(P, list(symbol_indexes.keys()), "text002")
            np.savetxt("text002_transition_matrix.csv", formatted_arr, delimiter=",", fmt='%s')

        # Jednotková matice
        I = np.eye(P_np_array.shape[0])

        # Vypočítat matici (PT - I)
        PT_minus_I = np.transpose(P_np_array) - I

        # Získat nulový prostor pomocí SVD rozkladu
        _, _, V = np.linalg.svd(PT_minus_I)

        # Nulový prostor je poslední sloupec matici V
        kernel_vector = V[-1]

        # Normalizovat vektor na jednotkový vektor
        stationary_vector = kernel_vector / np.sum(kernel_vector)

        # Převést na řádkový vektor a získat reálné hodnoty
        stationary_vector = np.real_if_close(np.transpose(stationary_vector))

        if isText1:
            pi1 = stationary_vector

        print("Stationary vector:")
        print(stationary_vector)
        print("Sum of stationary vector:")
        print(sum(stationary_vector))
        print("Product of stationary vector and P (should be again st.vec.")
        print(stationary_vector @ P_np_array)

        isText1 = False

    elems_to_merge = np.array([pi1[16] + pi1[23] + pi1[25]])
    merged_stacionary_vector = np.concatenate(
        (pi1[:16], pi1[17:23], pi1[24:25], pi1[26:], elems_to_merge))
    for ix in range(merged_stacionary_vector.size):
        merged_stacionary_vector[ix] = merged_stacionary_vector[ix] * chars_count

    print("stationary vector with merged low prob. values")
    print(merged_stacionary_vector)


    char_freq_to_merge = np.array([char_freq['q'] + char_freq['x'] + char_freq['z']])
    merged_char_freq_vector = np.concatenate(
        (list(char_freq.values())[:16], list(char_freq.values())[17:23], list(char_freq.values())[24:25], list(char_freq.values())[26:], char_freq_to_merge))

    print("character frequencies with merged low prob. values")
    print(merged_char_freq_vector)

    chi2_stat, p_val = chisquare(merged_char_freq_vector, f_exp=merged_stacionary_vector)

    print("chi2 stat:")
    print(chi2_stat)
    print("p value:")
    print(p_val)
