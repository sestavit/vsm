import sys
import string
import math
import matplotlib.pyplot as plt
import huffman


def visualize_probs(keys: list, values: list):
    plt.bar(keys, values, color='skyblue')
    plt.xlabel('Symbol')
    plt.ylabel('p')
    plt.title(f'Symbols probabilities in text')
    plt.show()


if __name__ == "__main__":
    birth_day = int(sys.argv[1])
    lastname_len = len(sys.argv[2])

    X = ((birth_day*lastname_len * 23) % 20) + 1
    Y = ((X + ((birth_day * 5 + lastname_len * 7) % 19)) % 20) + 1

    text1 = ""
    text2 = ""

    with open(f"../files/{f'00{X}.txt' if X < 10 else f'0{X}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text1 = f.read()

    with open(f"../files/0{f'0{Y}.txt' if Y < 10 else f'{Y}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text2 = f.read()

    all_symbols = string.ascii_lowercase + ' '

    t1_encoding, t2_encoding = None, None

    for text in [text1, text2]:
        char_freq = dict.fromkeys(all_symbols, 0)
        chars_count = 0

        # compute frequencies of characters (case-insensitive)
        for char in text:
            chars_count += 1
            if char in string.ascii_uppercase:
                char = char.lower()
            char_freq[char] += 1

        # normalize frequencies to get probabilities
        for key in char_freq.keys():
            char_freq[key] /= chars_count

        # filtered dict items with p > 0
        filtered_char_freq = {char: p for char, p in char_freq.items() if p > 0}

        # visualize probs
        visualize_probs(list(filtered_char_freq.keys()), list(filtered_char_freq.values()))

        # compute entropy
        entropy = -sum(p * math.log2(p) for p in filtered_char_freq.values())
        print(f"Entropy: {entropy}b")

        # construct Huffman encoding
        encoding = huffman.codebook(list(char_freq.items()))
        print(encoding)

        # compute expected length
        L = sum([len(code) * char_freq[char] for char, code in encoding.items()])
        print(f"L: {L}b")

        if text == text1:
            t1_encoding = encoding
        else:
            L = sum([len(code) * char_freq[char] for char, code in t1_encoding.items()])
            print(f"L of text2 with encoding from text1: {L}b")

