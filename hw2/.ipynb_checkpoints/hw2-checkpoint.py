import math
import sys
import matplotlib.pyplot as plt
from scipy.stats import chi2_contingency, ttest_ind_from_stats


def visualize_probs(keys: list, values: list):
    bars = plt.bar(keys, values, color='skyblue')
    plt.xticks(keys, [str(k) for k in keys])

    for bar in bars:
        y_val = bar.get_height()
        plt.text(bar.get_x() + bar.get_width() / 2, y_val, round(y_val, 2), va='bottom', ha='center')

    plt.xlabel('Word length')
    plt.ylabel('p')
    plt.title(f'Word length probabilities in text')
    plt.show()


if __name__ == "__main__":
    birth_day = int(sys.argv[1])
    lastname_len = len(sys.argv[2])

    X = ((birth_day * lastname_len * 23) % 20) + 1
    Y = ((X + ((birth_day * 5 + lastname_len * 7) % 19)) % 20) + 1

    text1 = ""
    text2 = ""

    with open(f"../files/{f'00{X}.txt' if X < 10 else f'0{X}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text1 = f.read()

    with open(f"../files/0{f'0{Y}.txt' if Y < 10 else f'{Y}.txt'}", encoding="UTF-8") as f:
        f.readline()
        text2 = f.read()

    print("==================== TEXT1 ==========================")

    # text 1
    words1 = text1.split(" ")
    words1_len_freq = dict.fromkeys(range(1, 20), 0)

    for word in words1:
        w_len = len(word)
        words1_len_freq[w_len] += 1

    # absolute counts
    words1_len_abs_freq = words1_len_freq.copy()
    print(f"Length counts: {words1_len_abs_freq}")

    # normalize frequencies to get probabilities
    for key in words1_len_freq.keys():
        words1_len_freq[key] /= len(words1)

    # relative counts
    print(f"Length probs: {words1_len_freq}")

    # visualize probs
    visualize_probs(list(words1_len_freq.keys()), list(words1_len_freq.values()))

    # expected value of word length
    EX_1 = sum([wl * prob for wl, prob in words1_len_freq.items()])
    print(f"EX1={EX_1}")

    # variance
    varX_1 = sum([((wl - EX_1) ** 2) * prob for wl, prob in words1_len_freq.items()])
    print(f"varX1={varX_1}")

    print("==================== TEXT2 ==========================")

    # text 2
    words2 = text2.split(" ")
    words2_len_freq = dict.fromkeys(range(1, 20), 0)

    for word in words2:
        w_len = len(word)
        words2_len_freq[w_len] += 1

    # absolute counts
    words2_len_abs_freq = words2_len_freq.copy()
    print(f"Length counts: {words2_len_abs_freq}")

    # normalize frequencies to get probabilities
    for key in words2_len_freq.keys():
        words2_len_freq[key] /= len(words2)

    # relative counts
    print(f"Length probs: {words2_len_freq}")

    # visualize probs
    visualize_probs(list(words2_len_freq.keys()), list(words2_len_freq.values()))

    # expected value of word length
    EX_2 = sum([wl * prob for wl, prob in words2_len_freq.items()])
    print(f"EX2={EX_2}")

    # variance
    varX_2 = sum([((wl - EX_2) ** 2) * prob for wl, prob in words2_len_freq.items()])
    print(f"varX2={varX_2}")

    # test hypothesis that word length distribution is independent
    alpha = .05

    # since we use chi-square test we cannot work with 0 cells, so we aggregate the less probable cells into single one
    row1 = list(words1_len_abs_freq.values())[:10]
    row1.append(sum(list(words1_len_abs_freq.values())[10:]))

    row2 = list(words2_len_abs_freq.values())[:10]
    row2.append(sum(list(words2_len_abs_freq.values())[10:]))

    cont_table = [row1, row2]

    print("==================== Chi square test ==========================")

    print(f"Contingents table: {cont_table}")

    # Test nezávislosti v kontingenční tabulce je statistický test,
    # který se používá ke zjištění, zda existuje vztah mezi dvěma kategoriálními proměnnými.
    # Pro testování hypotézy, že rozdělení délek slov nezávisí na tom, o který text se jedná,
    # použijeme chi-kvadrát test nezávislosti.
    #
    # Hypotéza, kterou testujeme, je:
    #
    # Nulová hypotéza Ho : Rozdělení délek slov je nezávislé na konkrétním textu.
    # Alternativní hypotéza Ha: Rozdělení délek slov je závislé na konkrétním textu.

    # chi_square statistic
    chi2_stat, p_value, dof, expected = chi2_contingency(cont_table)
    print("Chi2 Stat:", chi2_stat)
    print("Degrees of freedom:", dof)
    print("P-value:", p_value)
    print("Expected Frequencies:", expected)

    print(f"alpha={alpha}, p-value={p_value}")
    print(f'We reject Ho in favor of Ha.' if p_value < alpha else 'We do not reject Ho in favor of Ha')

    print("==================== 2 pair t-test test ==========================")

    # jako posledni mame dvouvyberovy t-test. Mejme náhodný výber X1, . . . , Xn z normálního rozdelení N (µ1, σ1_2)
    # a nezávislý náhodnýb výber Y1, . . . , Ym z normálního rozdelení N (µ2, σ2_2)
    # Chceme testovat hypotézu H0 : µ1 = µ2 proti HA : µ1 ̸= µ2.

    # => studentovo rozdeleni

    # kriticke hodnoty studentova rozdeleni
    total_words_text1 = sum(words1_len_abs_freq.values())
    total_words_text2 = sum(words2_len_abs_freq.values())

    t_stat, p_value = ttest_ind_from_stats(
        mean1=EX_1,
        std1=math.sqrt(varX_1),
        nobs1=total_words_text1,
        mean2=EX_2,
        std2=math.sqrt(varX_2),
        nobs2=total_words_text2,
        equal_var=False
    )

    print("t-stat:", t_stat)
    print("p-value:", p_value)
    print(f"We reject H0 for alpha={alpha}" if p_value < alpha else f"We do not reject H0 for alpha={alpha}")
