import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


if __name__ == '__main__':

    lamb = 10
    t_max = 10

    # generate arrivals
    t = 0
    arrival_times = []

    while t < t_max:
        t += np.random.exponential(1 / lamb)
        if t < t_max:
            arrival_times.append(t)

    # generate service times
    service_times = np.random.gamma(shape=2, scale=0.25, size=len(arrival_times))

    # times when requests are served
    served_times = [arrival_times[i] + service_times[i] for i in range(len(arrival_times))]

    # simulate 1 trajectory for t=(0,10s)
    print(f"Arrival times:\n{arrival_times}")
    print(f"Num of arrivals: {len(arrival_times)}")

    print(f"Service times:\n{service_times}")
    print(f"Mean service time: {np.mean(service_times)}")

    # Simulation of N_t
    times = sorted(arrival_times + served_times)
    Nt = []
    current_customers = 0

    for time in times:
        if time in arrival_times:
            current_customers += 1
        else:
            current_customers -= 1
        Nt.append(current_customers)

    # visualize
    plt.step(times, Nt, where='post')
    plt.title('Simulace stavu systému M|G|∞ v čase t ∈ (0, 10s)')
    plt.xlabel('Čas (s)')
    plt.ylabel('Počet zákazníků v systému')
    plt.grid(True)
    plt.show()

    # simulate n=500 trajectories for t=(0,100s)
    t_max = 100
    N_100 = []

    for _ in range(500):
        t = 0
        current_customers = 0
        arrival_times = []

        while t < t_max:
            t += np.random.exponential(1 / lamb)
            if t < t_max:
                arrival_times.append(t)

        # generate service times
        service_times = np.random.gamma(shape=2, scale=0.25, size=len(arrival_times))

        # times when requests are served
        served_times = [arrival_times[i] + service_times[i] for i in range(len(arrival_times))]
        served_times_until_100s = list(filter(lambda st: st < t_max, served_times))
        times = sorted(arrival_times + served_times_until_100s)

        for time in times:
            if time in arrival_times:
                current_customers += 1
            else:
                current_customers -= 1

        N_100.append(current_customers)

    sample_mean = np.mean(N_100)
    sample_var = np.var(N_100)

    print(N_100)
    print(f"N_100 mean: {sample_mean}")

    expected_lambda = 5
    standard_error = np.sqrt(expected_lambda / len(N_100))
    z_score = (sample_mean - expected_lambda) / standard_error
    p_value = 2 * (1 - stats.norm.cdf(np.abs(z_score)))

    print(f'Z-Score: {z_score}')
    print(f'P-Value: {p_value}')

    poisson_p_value = stats.poisson.sf(sample_mean, 5)
    print(f"Poisson p-value {poisson_p_value}")
